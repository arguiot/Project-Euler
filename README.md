# Project-Euler

This GitHub project contains simple solutions to problem listed on Project Euler.

### Installing

Clone the GitHub repository
```
$ git clone https://github.com/lukalafaye/Project-Euler
```

## Contributing

Please leave a star 🌟  to help the project getting more popular!

You can also read [CONTRIBUTING.md](https://github.com/lukalafaye/Project-Euler/blob/master/CONTRIBUTING.md) for details on submitting pull requests to this project.

## Authors

* **Luka Lafaye de Micheaux** - *Initial work* - [**lukalafaye**](https://github.com/lukalafaye)

## Files

### Python

| Euler 1 	| Euler 2 	| Euler 3 	| Euler 4 	| Euler 5 	| Euler 6 	| Euler 7 	| Euler 8 	| Euler 9 	| Euler 10 	| Euler 11 	| Euler 12 	| Euler 13 	| Euler 14 	| Euler 15 	| Euler 16 	| Euler 17 	| Euler 18 	| Euler 19 	| Euler 20 	|
|---------	|---------	|---------	|---------	|---------	|---------	|---------	|---------	|---------	|----------	|----------	|----------	|----------	|----------	|----------	|----------	|----------	|----------	|----------	|----------	|
|[euler1.py](https://github.com/lukalafaye/Project-Euler/blob/master/src/euler1.py)|[euler2.py](https://github.com/lukalafaye/Project-Euler/blob/master/src/euler2.py)|[euler3.py](https://github.com/lukalafaye/Project-Euler/blob/master/src/euler3.py)|[euler4.py](https://github.com/lukalafaye/Project-Euler/blob/master/src/euler4.py)|[euler5.py](https://github.com/lukalafaye/Project-Euler/blob/master/src/euler5.py)|[euler6.py](https://github.com/lukalafaye/Project-Euler/blob/master/src/euler6.py)|[euler7.py](https://github.com/lukalafaye/Project-Euler/blob/master/src/euler7.py)|[euler8.py](https://github.com/lukalafaye/Project-Euler/blob/master/src/euler8.py)|[euler9.py](https://github.com/lukalafaye/Project-Euler/blob/master/src/euler9.py)|[euler10.py](https://github.com/lukalafaye/Project-Euler/blob/master/src/euler10.py)|[euler11.py](https://github.com/lukalafaye/Project-Euler/blob/master/src/euler11.py)|[euler12.py](https://github.com/lukalafaye/Project-Euler/blob/master/src/euler12.py)|[euler13.py](https://github.com/lukalafaye/Project-Euler/blob/master/src/euler13.py)|[euler14.py](https://github.com/lukalafaye/Project-Euler/blob/master/src/euler14.py)|[euler15.py](https://github.com/lukalafaye/Project-Euler/blob/master/src/euler15.py)|[euler16.py](https://github.com/lukalafaye/Project-Euler/blob/master/src/euler16.py)|[euler17.py](https://github.com/lukalafaye/Project-Euler/blob/master/src/euler17.py)|[euler18.py](https://github.com/lukalafaye/Project-Euler/blob/master/src/euler18.py)|[euler19.py](https://github.com/lukalafaye/Project-Euler/blob/master/src/euler19.py)|[euler20.py](https://github.com/lukalafaye/Project-Euler/blob/master/src/euler20.py)|
